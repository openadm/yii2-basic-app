<?php

$params = array_merge(
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'openadm-console',
    'bootstrap' => ['log', 'gii'],
    'basePath' => '@app',
    'vendorPath' => '@vendor',
    'runtimePath' => '@runtime',
    //'controllerNamespace' => 'app\console',
    'controllerMap' => [
        'migrate' => [
            'class' => 'openadm\admin\console\MigrateController'
        ]
    ],
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
            'cache' => 'cache',
            'ruleTable' => '{{%auth_rule}}', // Optional
            'itemTable' => '{{%auth_item}}',  // Optional
            'itemChildTable' => '{{%auth_item_child}}',  // Optional
            'assignmentTable' => '{{%auth_assignment}}',  // Optional
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
    ],
    'modules' => [
	    'gii' => 'yii\gii\Module',
//        'admin' => [
//            'class' => 'openadm\admin\modules\admin\Module',
//        ],
//        'rbac' => [
//            'class' => 'yii2mod\rbac\Module',
////            'as access' => [
////                'class' => yii2mod\rbac\filters\AccessControl::class
////            ],
//            'controllerMap' => [
//                'assignment' => [
//                    'class' => 'yii2mod\rbac\controllers\AssignmentController',
//                ],
//                'role' => [
//                    'class' => 'openadm\admin\modules\rbac\controllers\RoleController',
//                ],
//                'route' => [
//                    'class' => 'openadm\admin\modules\rbac\controllers\RouteController',
//                ],
//            ],
//        ],
	],
		
    'params' => $params,
];
