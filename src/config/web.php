<?php
$params = yii\helpers\ArrayHelper::merge(
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);
$config = [
    'id' => 'openadm',
    'basePath' => '@app',
    'vendorPath' => '@vendor',
    'runtimePath' => '@runtime',
    'bootstrap' => ['log'],
    'name' => 'OpenAdm',
    'language'=>'zh-CN',
    'sourceLanguage' => 'en-US',
    'TimeZone' => 'PRC',
    'defaultRoute' => 'site/index',
    'components' => [
        //有反向代理的情况下,要配置trustedHosts否则不能获取真实的https状态，跳转会失败
//        'request' => [
//            'trustedHosts' => [
//                '192.168.0.0/8'=> [
//                    'X-Forwarded-For',
//                    'X-Forwarded-Host',
//                    'X-Forwarded-Proto',
//                    'X-Proxy-User-Ip',
//                    'Front-End-Https',
//                ],
//            ],
//        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => false,
            'rules'=>[
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
    ],
    'modules' => [

    ],
    'params' => $params,
];
if(is_file(__DIR__ . '/web-ext.php')){
    return yii\helpers\ArrayHelper::merge(
        require(\yii::getAlias('@vendor'). '/openadm/yii2-admin/config/openadm.php'),
        require(__DIR__ . '/web-ext.php'),
        $config
    );
}else{
    return yii\helpers\ArrayHelper::merge(
        require(\yii::getAlias('@vendor'). '/openadm/yii2-admin/config/openadm.php'),
        $config
    );
}

