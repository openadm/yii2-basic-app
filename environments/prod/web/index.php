<?php
defined('YII_DEBUG') or define('YII_DEBUG', false);
defined('YII_ENV') or define('YII_ENV', 'prod');

require(__DIR__ . '/../vendor/autoload.php');
require(__DIR__ . '/../vendor/yiisoft/yii2/Yii.php');
require(__DIR__ . '/../src/config/bootstrap.php');

$config = yii\helpers\ArrayHelper::merge(
    require(__DIR__ . '/../src/config/web.php'),
    require(__DIR__ . '/../src/config/web-local.php'),
    require(__DIR__ . '/../src/config/db.php')
);

$application = new yii\web\Application($config);
$application->run();
